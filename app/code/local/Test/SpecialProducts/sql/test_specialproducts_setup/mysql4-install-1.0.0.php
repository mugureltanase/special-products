<?php
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();

/**
 * Create table 'test_specialproducts_special'
 */
$table = $installer->getConnection()
    // The following call to getTable('test_specialproducts/special') will lookup the resource for test_specialproducts (test_specialproducts_mysql4), and look
    // for a corresponding entity called special. The table name in the XML is specialproducts, so ths is what is created.
    ->newTable($installer->getTable('test_specialproducts/special'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'ID')
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_CLOB, 0, array(
        'nullable'  => false,
    ), 'Name')
    ->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, array(
        'nullable'  => false,
    ), 'Product Id')
    ->addColumn('products_number', Varien_Db_Ddl_Table::TYPE_INTEGER, array(
        'nullable'  => false,
    ), 'Number of products')
    ->addColumn('category', Varien_Db_Ddl_Table::TYPE_VARCHAR, 128, array(
        'nullable'  => false,
    ), 'Category')
    ->addColumn('starting_date', Varien_Db_Ddl_Table::TYPE_DATE, array(
        'nullable'  => false,
    ), 'Starting Date')
    ->addColumn('end_date', Varien_Db_Ddl_Table::TYPE_DATE, array(
        'nullable'  => false,
    ), 'End Date')
;
$installer->getConnection()->createTable($table);

$installer->endSetup();