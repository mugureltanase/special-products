<?php

/**
 * Class Test_SpecialProducts_Model_Special
 */
class Test_SpecialProducts_Model_Special extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('test_specialproducts/special');
    }
}