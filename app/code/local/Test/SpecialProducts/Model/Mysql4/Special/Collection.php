<?php

/**
 * Class Test_SpecialProducts_Model_Mysql4_Special_Collection
 */
class Test_SpecialProducts_Model_Mysql4_Special_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('test_specialproducts/special');
    }
}