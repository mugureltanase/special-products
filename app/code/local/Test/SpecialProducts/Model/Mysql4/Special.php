<?php

/**
 * Class Test_SpecialProducts_Model_Mysql4_Special
 */
class Test_SpecialProducts_Model_Mysql4_Special extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('test_specialproducts/special', 'id');
    }
}