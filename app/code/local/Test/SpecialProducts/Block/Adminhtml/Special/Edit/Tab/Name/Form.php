<?php

/**
 * Class Test_SpecialProducts_Block_Adminhtml_Special_Edit_Tab_Name_Form
 */

class Test_SpecialProducts_Block_Adminhtml_Special_Edit_Tab_Name_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('special_name_form',
            array('legend'=>'Add Special Products'));
        $fieldset->addField('name', 'text',
            array(
                'label' =>Mage::helper('test_specialproducts')->__('Name'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'name',
            ));


        $fieldset->addField('starting_date', 'text', array(
            'name'      => 'start_date',
            'label'     => Mage::helper('test_specialproducts')->__('Start Date'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'class'     => 'validate-date validate-date-range date-range-custom_theme-from'
        ));


        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );

        $fieldset->addField('end_date', 'date', array(
            'name'      => 'end_date',
            'label'     => Mage::helper('test_specialproducts')->__('End Date'),
            'image'     => $this->getSkinUrl('images/grid-cal.gif'),
            'format'    => $dateFormatIso,
            'class'     => 'validate-date validate-date-range date-range-custom_theme-from'
        ));


        $fieldset->addField('category', 'select',
            array(
                'label' => 'Category',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'category',
            ));

        $fieldset->addField('number_products', 'text',
            array(
                'label' => Mage::helper('test_specialproducts')->__('Number of Products'),
                'class' => 'required-entry',
                'required' => true,
                'name' => 'number_products',
            ));

        if ( Mage::registry('test_specialproducts') )
        {
            $form->setValues(Mage::registry('test_specialproducts')->getData());
        }

        return parent::_prepareForm();
    }
}