<?php

/**
 * Class Test_SpecialProducts_Block_Adminhtml_Special_Edit_Tabs
 */

class Test_SpecialProducts_Block_Adminhtml_Special_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('test_sepcialproducts_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Info');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('add_special_products_section', array(
            'label' => Mage::helper('test_specialproducts')->__('Add Special Products'),
            'title' => Mage::helper('test_specialproducts')->__('Add Special Products'),
            'content' => $this->getLayout()
                ->createBlock('test_specialproducts/adminhtml_special_edit_tab_name_form')
                ->toHtml()
        ));
        $this->addTab('add_category_section', array(
            'label' =>Mage::helper('test_specialproducts')->__('Available Products'),
            'title' =>Mage::helper('test_specialproducts')->__('Available Products'),
            'content' => $this->getLayout()
                ->createBlock('test_specialproducts/adminhtml_special_edit_tab_available_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}