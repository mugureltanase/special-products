<?php

/**
 * Class Test_SpecialProducts_Block_Adminhtml_Special_Edit
 */

class Test_SpecialProducts_Block_Adminhtml_Special_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        parent::__construct();
        $this->_blockGroup = 'test_specialproducts';
        $this->_controller = 'adminhtml_special';
        $this->_updateButton('save', 'label', $this->__('Save Special'));
        $this->_updateButton('delete', 'label', $this->__('Delete Special'));
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('test_specialproducts')->getId()) {
            return $this->__('Edit Special');
        }
        else {
            return $this->__('New Special');
        }
    }
}