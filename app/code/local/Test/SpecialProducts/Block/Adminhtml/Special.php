<?php

/**
 * Class Test_SpecialProducts_Block_Adminhtml_SpecialProducts
 */
class Test_SpecialProducts_Block_Adminhtml_Special extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        // The blockGroup must match the first half of how we call the block, and controller matches the second half
        // ie. test_specialproducts/adminhtml_special
        
        $this->_blockGroup = 'test_specialproducts';
        $this->_controller = 'adminhtml_special';
        $this->_headerText = Mage::helper('test_specialproducts')->__('Special');

        parent::__construct();
    }
}