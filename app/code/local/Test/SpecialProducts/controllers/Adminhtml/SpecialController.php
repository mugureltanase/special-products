<?php

/**
 * Class Test_SpecialProducts_Adminhtml_SpecialProductsController
 */

class Test_SpecialProducts_Adminhtml_SpecialController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {

        // Let's call our initAction method which will set some basic params for each action
        $this->_initAction()
            ->renderLayout();
    }

    public function newAction()
    {
        // We just forward the new action to a blank edit form
        $this->_forward('edit');
    }

    public function editAction()
    {
        $this->_initAction();

        // Get id if available
        $id  = $this->getRequest()->getParam('id');
        $model = Mage::getModel('test_specialproducts/special');

        if ($id) {
            // Load record
            $model->load($id);
            // Check if record is loaded
            if (!$model->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This Special products no longer exists.'));
                $this->_redirect('*/*/');

                return;
            }
        }

        $this->_title($model->getId() ? $model->getName() : $this->__('New Special'));
        $data = Mage::getSingleton('adminhtml/session')->getSpecialData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        Mage::register('test_specialproducts', $model);
        $this->_initAction();
        $this->_addBreadcrumb($id ? $this->__('Edit Special') : $this->__('New Special'), $id ? $this->__('Edit Special') : $this->__('New Special'));
        $this->_addContent($this->getLayout()
                 ->createBlock('test_specialproducts/adminhtml_special_edit'))
                 ->_addLeft($this->getLayout()
                     ->createBlock('test_specialproducts/adminhtml_special_edit_tabs')
                 );
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($postData = $this->getRequest()->getPost()) {
            $model = Mage::getSingleton('test_specialproducts/special');
            $model->setData($postData);

            try {
                $model->save();

                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The Special products has been saved.'));
                $this->_redirect('*/*/');

                return;
            }
            catch (Mage_Core_Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
            catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this Special products.'));
            }

            Mage::getSingleton('adminhtml/session')->setSpecialData($postData);
            $this->_redirectReferer();
        }
    }


    public function deleteAction()
    {
        if($this->getRequest()->getParam('id') > 0)
        {
            try
            {
                $filmsModel = Mage::getModel('test_specialproducts/special');
                $filmsModel->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('successfully deleted');
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    private function _initAction()
    {
        $this->loadLayout()
            // Make the active menu match the menu config nodes (without 'children' inbetween)
            ->_setActiveMenu('catalog/test_specialproducts_special')
            ->_title($this->__('Catalog'))->_title($this->__('Special'))
            ->_addBreadcrumb($this->__('Catalog'), $this->__('Catalog'))
            ->_addBreadcrumb($this->__('Special'), $this->__('Special'));

        return $this;
    }

    /**
     * Check currently called action by permissions for current user
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/test_specialproducts_special');
    }
}